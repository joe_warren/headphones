{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE ScopedTypeVariables #-}
module Headphones (cupPart) where

import qualified Csg
import Control.Monad

zCylinder = Csg.translate (0, 0, 0.5) $ Csg.unitCylinder 32
zCube = Csg.translate (0, 0, 0.5) Csg.unitCube
xzCube = Csg.translate (0.5, 0, 0.5) Csg.unitCube
disk radius height = Csg.scale (radius, radius, height) $ zCylinder


bottomDisk = disk radius 2 
  where
    radius = 66/2

cowlingDisk = (disk radius 9) `Csg.subtract` (disk innerRadius 12)
  where
    radius = 60/2
    innerRadius = 49.5/2

cowlingHoles = Csg.unionConcat $ cableSlot : axel : axelSlit : axelSlots
  where
    cableSlot = Csg.translate (60/2, 0, 0) $ Csg.scale (20, 2, 20) Csg.unitCube
    axel = Csg.translate (0, 0, 2 + (3/2) + 0.5) $
            Csg.rotate (1, 0, 0) (pi/2) $ 
                Csg.scale (3/2, 3/2, 55) $ Csg.unitCylinder 8
    axelSlit = Csg.translate (0, 0, 2 + 0.5 + 3/2) $ Csg.scale (1, 55, 20) zCube 
    axelSlot = Csg.translate (0, 0, 2 + 0.5) $ Csg.scale (8, 2, 20) zCube 
    axelSlots = (\i -> Csg.translate (0, i * 54/2, 0) axelSlot) <$> [-1, 1]


addCurveSegment :: Double -> Csg.BspTree -> Csg.BspTree -> Csg.BspTree
addCurveSegment angle object segment = object `Csg.union` finalSegment
  where
    ((_,_,_),(offsetA,_,_)) = Csg.aabb object
    ((offsetB,_,_),(_,_,_)) = Csg.aabb segment
    positionedSegment = Csg.translate (-offsetB, 0, 0) segment
    rotatedSegment = Csg.rotate (0, -1, 0) angle positionedSegment
    finalSegment = Csg.translate (offsetA,0, 0) rotatedSegment


oneCurveStep = (Csg.translate (10, 0, 0)).(Csg.rotate (0, -1, 0) angle)
  where 
    angle = pi/(4*6)

genericCurveSegment :: Csg.BspTree -> Csg.BspTree
genericCurveSegment segment = Csg.unionConcat $ take 6 $ iterate oneCurveStep segment

curveSegment :: Csg.BspTree
curveSegment = genericCurveSegment $ Csg.scale (10, 30, 9) xzCube

skinnyCurveSegment = genericCurveSegment $ Csg.scale (10, 30, 2) xzCube

positionHinge :: Csg.BspTree -> Csg.BspTree
positionHinge hinge = (!! 6) $ iterate oneCurveStep $ 
                        Csg.rotate (0, 1, 0) (pi/(4*6)) $ hinge

unpositionedHinge = Csg.translate (0, 0, 9 - hingeThickness) $ allParts
  where
    hingeDist = 8 -- confirmThis
    hingeThickness = 5 -- and thi
    hingeWidth = 14 -- and this
    holeRadius = 0.5
    cubePart = Csg.scale (hingeDist, hingeWidth, hingeThickness) xzCube 
    cylinderPart = Csg.translate (hingeDist, 0, hingeThickness/2) $
                    Csg.rotate (1, 0, 0) (pi/2) $
                     Csg.scale (hingeThickness/2, hingeThickness/2, hingeWidth) $
                      Csg.unitCylinder 16
    holePart = Csg.translate (hingeDist, 0, hingeThickness/2) $
                    Csg.rotate (1, 0, 0) (pi/2) $
                     Csg.scale (holeRadius, holeRadius, hingeWidth*2) $
                      Csg.unitCylinder 8
    slitPart = Csg.scale (100, 2, 100) Csg.unitCube
    allParts = (cubePart `Csg.union` cylinderPart) `Csg.subtract` (holePart `Csg.union` slitPart)

curveyBit :: Double -> Double  -> Csg.BspTree -> Csg.BspTree
curveyBit radius height band = basis `Csg.subtract` bothCylinders
  where
    bandMask = Csg.translate (xCutaway, 0, 0) $ Csg.scale (radius*5, radius*5, radius*5)  $ Csg.translate (0.5,0,0) $ zCube
    widerBand = Csg.scale (1, 3, 1) band
    flat = Csg.scale (xCutaway/2 + 35/2 + 0.1, yCutaway, height) $ Csg.translate (-0.5,0,0) $ zCube
    basis = (((widerBand `Csg.subtract` bandMask) `Csg.union` flat) 
        `Csg.subtract` (Csg.translate (-35, 0, 0) (disk 30 20)))

    yCutaway = 15 + radius
    xCutaway = (sqrt ((4*radius*radius) - (yCutaway*yCutaway))) - 35
    cutawayCylinder = Csg.translate (xCutaway, yCutaway, 0) $
            Csg.scale (radius, radius, 50) $ Csg.unitCylinder 32
    bothCylinders = cutawayCylinder `Csg.union` (Csg.scale (1, -1, 1) cutawayCylinder)

headband = Csg.translate (35,0, 0) $ 
            (Csg.unionConcat [tallerCurve, band, skinnyCurve, hinge]) 
             `Csg.subtract` tinySlit
  where 
    band = curveSegment
    tallerCurve = curveyBit (60/2) 9 band
    skinnyCurve = curveyBit (66/2) 2 skinnyCurveSegment
    hinge = positionHinge unpositionedHinge
    tinySlit = Csg.translate (-5, 0, 2) $
                Csg.rotate (0, -1, 0) (pi/4) $
                 Csg.scale (60, 2, 60) $ 
                  Csg.translate(0.5, 0, 0.5) Csg.unitCube

cupPart = headband `Csg.union` bottomDisk `Csg.union` (cowlingDisk `Csg.subtract` cowlingHoles)
