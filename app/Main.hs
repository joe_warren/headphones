{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE ScopedTypeVariables #-}
module Main where

import qualified Csg
import qualified Csg.STL
import qualified Data.Text.IO as T

import qualified Headphones

writeObject :: Csg.BspTree -> String -> IO ()
writeObject obj filename = do
    putStrLn $ "writing " ++ filename
    T.writeFile filename $ Csg.STL.toSTL obj
    putStrLn $ "written " ++ filename

main :: IO [()]
main = sequence $ (uncurry writeObject) <$> [
        (Headphones.cupPart, "headphone_cup_part.stl")
    ]
